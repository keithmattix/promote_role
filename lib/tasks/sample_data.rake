namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do
    make_role_groups
    make_roles
    make_users
  end
end



def make_role_groups
  RoleGroup.create!(is_temporary: false, name: "Base", parent_id: nil)
  RoleGroup.create!(is_temporary: false, name: "Manager", parent_id:1)
end

def make_roles
  Role.create!(name: "IT Manager", group_id: "2")
end

def make_users
  User.create!(name: "Keith Mattix", email: "mystic77892@gmail.com",
                       password: "mathofmattix",
                       password_confirmation: "mathofmattix", role_id: 1)
end
