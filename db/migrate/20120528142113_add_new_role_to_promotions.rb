class AddNewRoleToPromotions < ActiveRecord::Migration
  def change
    add_column :promotions, :new_role, :integer
  end
end
