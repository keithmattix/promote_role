class CreatePromotions < ActiveRecord::Migration
  def change
    create_table :promotions do |t|
      t.datetime :start_date
      t.datetime :expiration_date
      t.integer :user_id

      t.timestamps
    end
  end
end
