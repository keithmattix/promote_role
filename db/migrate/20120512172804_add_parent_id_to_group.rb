class AddParentIdToGroup < ActiveRecord::Migration
  def change
    add_column :role_groups, :parent_id, :integer
  end
end
