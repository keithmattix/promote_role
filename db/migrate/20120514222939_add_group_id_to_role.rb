class AddGroupIdToRole < ActiveRecord::Migration
  def change
    add_column :roles, :group_id, :integer
  end
end
