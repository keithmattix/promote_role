class MakeDefaultTempBoolFalse < ActiveRecord::Migration
  
  def change 
    change_column :roles, :is_temporary, :boolean, default: false 
  end	
end
