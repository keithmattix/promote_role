class CreateRoleGroups < ActiveRecord::Migration
  def change
    create_table :role_groups do |t|
      t.string :name
      t.binary :permissions
      t.boolean :is_temporary

      t.timestamps
    end
  end
end
