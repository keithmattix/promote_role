class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.boolean :is_temporary
      t.string :name
      t.integer :authorizing_user_id

      t.timestamps
    end
  end
end
