require 'spec_helper'

describe RoleGroup do
  
  describe "parents" do
    before do
      @parent_group = RoleGroup.create(is_temporary:false, name: "Parent Group", parent_id: nil)
      @child_group = RoleGroup.create(is_temporary:false, name: "Child Group", parent_id: @parent_group.id)
      @child_group2 = RoleGroup.create(is_temporary:false, name: "Child Group2", parent_id: @child_group.id)
      @child_group3 = RoleGroup.create(is_temporary:false, name: "Child Group3", parent_id: @parent_group.id)
      @child_group4 = RoleGroup.create(is_temporary:false, name: "Child Group4", parent_id: @child_group.id)
    end
  it "should know their children" do
      test_groups = @parent_group.children
      test_groups.should include(@child_group) 
      test_groups.should include(@child_group2) 
      test_groups.should include(@child_group3)
      test_groups.should include(@child_group4)
      test_groups = @child_group.children
      test_groups.should include(@child_group2) 
      test_groups.should_not include(@child_group3) 
      test_groups.should include(@child_group4)
    end
  end
end