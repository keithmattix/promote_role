require 'spec_helper'

describe Role do
  
  before do
    @role = Role.create(name: "IT Specialist", is_temporary: true, authorizing_user_id: 1, group_id: 1)
  end
  
  subject { @role }
  
  it { should respond_to(:name) }
  it { should respond_to(:token) }

end
