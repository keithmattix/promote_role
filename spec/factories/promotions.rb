# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :promotion do
    start_date "2012-05-28 09:13:33"
    expiration_date "2012-05-28 09:13:33"
    user_id 1
  end
end
