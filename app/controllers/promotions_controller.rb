class PromotionsController < ApplicationController
  
  def create
    convert_dates_to_ints
    @promotion = Promotion.new(params[:promotion])
    @promotion.user_id = params[:user_id]
    if @promotion.save
      @user = User.find(@promotion.user_id)
      @user.update_attribute(:role_id, @promotion.new_role)
      flash[:success] = "#{User.find(params[:user_id]).name} has been promoted"
      redirect_to User.find(params[:user_id])
    else 
      render 'users#promote'
    end
  end

  def destroy
  end

  def show
  end

  def edit
  end

  def index
  end
  
  private 
    def convert_dates_to_ints
     @start_date_year = Integer(params[:promotion]["start_date(1i)"])
     @start_date_date = Integer(params[:promotion]["start_date(3i)"])
     @start_date_month = Integer(params[:promotion]["start_date(2i)"])
     @start_date = Date.new(@start_date_year, @start_date_month, @start_date_date)
     params[:promotion][:start_date] = @start_date
     @expiration_date_year = params[:promotion]["expiration_date(1i)"]
     @expiration_date_date = params[:promotion]["expiration_date(3i)"]
     @expiration_date_month = params[:promotion]["expiration_date(2i)"]
     @expiration_date_year = Integer(@expiration_date_year) unless @expiration_date_year == ""
     @expiration_date_date = Integer(@expiration_date_date) unless @expiration_date_date == ""
     @expiration_date_month = Integer(@expiration_date_month) unless @expiration_date_month == ""
     @expiration_date_is_num = false
     @expiration_date_is_num  = true unless @expiration_date_year.class == String
     @expiration_date = Date.new(@expiration_date_year, @expiration_date_month, @expiration_date_date) unless @expiration_date_is_num == false
     params[:promotion][:expiration_date] = @expiration_date
    end
end
