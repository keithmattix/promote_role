class RolesController < ApplicationController
  before_filter :check_for_base, only: [:new]
  
  def new
    @role = Role.new
  end

  def create
    params[:role][:group_id] = Integer(params[:role][:group_id]) 
    @role = Role.new(params[:role])
    if @role.save
      redirect_to :action => "index"
    else
      render 'new'
    end
  end

  def index
    @roles = Role.all
  end
  def check_for_base
    if RoleGroup.find_by_name("Base").nil?
      RoleGroup.create(name: "Base", is_temporary: false, parent_id: nil)
    end
  end
end
