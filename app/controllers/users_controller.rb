class UsersController < ApplicationController
  before_filter :signed_in_user, only: [:promote, :index, :new]
  before_filter :check_manager, only: [:promote]
  
  def new
    @user = User.new
    usable_group = RoleGroup.find(current_user.role.group_id)
    @usable_roles = usable_group.members
    usable_group.parents.each do |e|
      e.members.each do |f|
	@usable_roles << f
      end
    end
  end

  def promote
    @user = User.find(params[:id])
    @promotion ||= current_user.promotions.build
    usable_group = RoleGroup.find(current_user.role.group_id)
    @usable_roles = usable_group.members
    usable_group.parents.each do |e|
      e.members.each do |f|
	@usable_roles << f
      end
    end
    @usable_roles = @usable_roles - [@user.role]
    @user.role.group.parents.each do |x|
      x.members.each do |y|
	@usable_roles = @usable_roles - [y]
      end
    end
    @current_user = current_user
  end

  def index
    @users = User.all
  end
    
  def show
    @user = User.find(params[:id])
  end
  
  def create
    params[:user][:role_id] = Role.find(Integer(params[:user][:role_id]))
    @user = User.new(params[:user])
    if @user.save
      sign_in @user
      flash[:success] = "Welcome to the Warehouse.com!"
      redirect_to @user
    else
      render 'new'
    end
  end
end
