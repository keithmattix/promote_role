class RoleGroupsController < ApplicationController
  before_filter :check_for_base, only: [:new]
  
  def new
    @role_group = RoleGroup.new
  end
  
  def create
    params[:role_group][:parent_id] = Integer(params[:role_group][:parent_id]) 
    @role_group= RoleGroup.new(params[:role_group])
    if @role_group.save
      redirect_to :action => "index"
    else
      render 'new'
    end
  end
  
  def index
    @role_groups = RoleGroup.all
  end
  
def check_for_base
    if RoleGroup.find_by_name("Base").nil?
      RoleGroup.create(name: "Base", is_temporary: false, parent_id: nil)
    end
  end
end
