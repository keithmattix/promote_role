class Promotion < ActiveRecord::Base
  attr_accessible :expiration_date, :start_date, :user_id, :new_role
  belongs_to :user

  def user
    User.find(self.user_id)
  end
end
