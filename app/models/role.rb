class Role < ActiveRecord::Base
  
  attr_accessible :authorizing_user_id, :is_temporary, :name, :token, :expiration_date, :group_id
  before_create :check_temporary
  belongs_to :role_group
  has_many :users
  
  def check_temporary
    if is_temporary?
      self.token = Digest::MD5::hexdigest("#{self.authorizing_user_id} #{self.name}")
    end
  end
  
  def group
    RoleGroup.find(self.group_id)
  end
  
  def member_of_group?(group_name)
    group_name = group_name.strip! unless group_name.strip! == nil
    role_group = self.group
    if(group_name == role_group.name)
      return true
    end
    self.group.parents.each do |g|
      if g.name == group_name
	return true
      end
    end
    return false
  end
    
 def groups
   group_array = Array.new
   group_array << self.group
   self.group.parents.each do |x|
    group_array << x
   end
   group_array
 end
end