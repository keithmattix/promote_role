class User < ActiveRecord::Base
  attr_accessible :name, :password, :email, :role_id, :password_confirmation
  belongs_to :role
  has_many :promotions
  has_secure_password
  
  before_create { |user| user.email = email.downcase }
  before_create :create_remember_token
  
  validates :name, presence: true, length: { maximum: 50 }, uniqueness: { case_sensitive: false }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence:   true,
                    format:     { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  validates :password, presence: true, length:  { minimum: 6 }, on: :create
  validates :role_id, presence: true
  validates :password_confirmation, presence: true
  
  def is_manager?
    user_role = Role.find(self.role_id)
    user_role_group_names = Array.new
    user_role.groups.each do |x|
     user_role_group_names << x.name
    end
    user_role_group_names.include?("Manager")
  end
  
  def role
    Role.find(self.role_id)
  end


    private
      
      def create_remember_token
	self.remember_token = SecureRandom.urlsafe_base64
      end 
end
