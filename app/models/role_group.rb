class RoleGroup < ActiveRecord::Base
  attr_accessible :is_temporary, :name, :permissions, :parent_id
  has_many :roles, foreign_key: "group_id"
  
  def children
    children_array = Array.new
    current_parent_group = self
    RoleGroup.where(parent_id: current_parent_group).each do |e|
      children_array << e
    end
    children_array.each do |e|
      children_array = children_array.concat(e.children)
    end
    children_array
  end 
  
  def members
    Role.where(group_id: self.id)
  end
  
  def parents
    parent_array = Array.new
    if(self.parent_id != nil)
     parent_array << RoleGroup.find(self.parent_id)
    else
      return parent_array.uniq
    end
    parent_array.first.parents.each do |x|
     parent_array << x
    end
    parent_array
  end
  
  def parent
    if(self.parent_id != nil)
      return RoleGroup.find(self.parent_id)
    else
      return nil
    end
  end
end